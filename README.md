## VIVY



## Code structure
I’m following the clean architecture, you can check the samples [here](https://fernandocejas.com/2018/05/07/architecting-android-reloaded/).



## Dependencies
1. Coroutines.
2. Lifecycle-ViewModel.
3. Retrofit.
4. Glide.
5. Koin.
6. Paging.

## Developed by

This project developed by Ali Abozaid you can find me [here](https://www.linkedin.com/in/aliabozaid/)
 


