package com.aliabozaid.vivy.data

import com.aliabozaid.vivy.base.UnitTest
import com.aliabozaid.vivy.data.executor.ApiExecutor
import com.aliabozaid.vivy.data.model.DoctorListModel
import com.aliabozaid.vivy.data.network.NetworkHandler
import com.aliabozaid.vivy.data.repository.DoctorsRepository
import com.aliabozaid.vivy.data.service.DoctorsService
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class DoctorsRepositoryTest : UnitTest() {
    private lateinit var doctorsRepository: DoctorsRepository

    @Mock
    private lateinit var apiExecutor: ApiExecutor<DoctorListModel>
    @Mock
    private lateinit var networkHandler: NetworkHandler

    @Mock
    private lateinit var doctorsService: DoctorsService

    @Mock
    private lateinit var doctorListModel: DoctorListModel

    @Before
    fun setUp() {
        doctorsRepository = DoctorsRepository(
            apiExecutor, networkHandler, doctorsService
        )
    }

    @Test
    fun `running getDoctors should use apiExecutor and get data from repository`() {
        val myScope = GlobalScope
        runBlocking {
            myScope.launch(Dispatchers.IO) {
                doctorsRepository.getDoctors(API_KEY)
                verify(apiExecutor).invoke { doctorListModel }
                verify(doctorsService).getDoctors(API_KEY)
            }
        }
    }

    companion object {
        private const val API_KEY: String = "API_KEY"
    }
}
