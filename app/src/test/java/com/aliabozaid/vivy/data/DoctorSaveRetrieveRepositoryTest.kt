package com.aliabozaid.vivy.data

import com.aliabozaid.vivy.base.UnitTest
import com.aliabozaid.vivy.data.model.DoctorModel
import com.aliabozaid.vivy.data.repository.DoctorSaveRetrieveRepository
import com.aliabozaid.vivy.data.service.DoctorDetailsService
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mock

class DoctorSaveRetrieveRepositoryTest : UnitTest() {
    private lateinit var doctorSaveRetrieveRepository: DoctorSaveRetrieveRepository

    @Mock
    private lateinit var doctorDetailsService: DoctorDetailsService

    @Mock
    private lateinit var doctorsModel: List<DoctorModel>

    @Test
    fun `running loadRecentSelectedDoctors should use loadRecentSelectedDoctors in doctorDetailsService`() {
        val myScope = GlobalScope
        runBlocking {
            myScope.launch(Dispatchers.IO) {
                doctorSaveRetrieveRepository.loadRecentSelectedDoctors()
                verify(doctorDetailsService).loadRecentSelectedDoctors()
            }
        }
    }

    @Test
    fun `running saveRecentSelectedDoctors should use saveRecentSelectedDoctors in doctorDetailsService`() {
        val myScope = GlobalScope
        runBlocking {
            myScope.launch(Dispatchers.IO) {
                doctorSaveRetrieveRepository.saveRecentSelectedDoctors(doctorsModel)
                verify(doctorDetailsService).saveRecentSelectedDoctors(doctorsModel)
            }
        }
    }
}
