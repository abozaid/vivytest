package com.aliabozaid.vivy.data

import com.aliabozaid.vivy.base.AndroidTest
import com.aliabozaid.vivy.data.cache.CacheManager
import com.google.gson.Gson
import java.io.File
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldEqual
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class CacheManagerTest : AndroidTest() {
    private lateinit var cacheManager: CacheManager
    private lateinit var file: File

    companion object {
        private const val KEY = "KEY"
        private const val WRONG_KEY = "WRONG_KEY"
        private const val SAVED_OBJECT = "SAVED_OBJECT"
    }

    @Before
    fun setUp() {
        cacheManager = CacheManager(context(), Gson())
        file = cacheManager.getCachedFile(KEY)
    }

    @After
    fun tearDown() {
        if (file.exists()) {
            file.delete()
        }
    }

    @Test
    fun `running saveDataAsync should create the file `() {
        runBlocking {
            cacheManager.saveDataAsync(
                KEY,
                SAVED_OBJECT
            )
        }
        Assert.assertTrue(file.exists())
    }

    @Test
    fun `running saveDataAsync and loadData for the same key should return the same value`() {
        runBlocking {
            cacheManager.saveDataAsync(
                KEY,
                SAVED_OBJECT
            )
            val loadedData = cacheManager.loadData<String>(KEY)
            loadedData shouldEqual SAVED_OBJECT
        }
    }

    @Test
    fun `running saveDataAsync and loadData for array with the same key should return the same value`() {
        val data = listOf(1, 2, 4)
        runBlocking {
            cacheManager.saveDataAsync(KEY, data)
            val loadData = cacheManager.loadData<Array<Int>>(KEY)
            Assert.assertArrayEquals(loadData?.toIntArray(), data.toIntArray())
        }
    }

    @Test
    fun `running loadData with wrong key should return null`() {
        runBlocking {
            val loadData = cacheManager.loadData<String>(WRONG_KEY)
            Assert.assertNull(loadData)
        }
    }
}
