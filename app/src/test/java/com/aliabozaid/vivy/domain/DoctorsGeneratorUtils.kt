package com.aliabozaid.vivy.domain

import com.aliabozaid.vivy.data.model.DoctorModel

object DoctorsGeneratorUtils {

    fun getDoctorsModel(): MutableList<DoctorModel> {
        val list = mutableListOf<DoctorModel>()
        list.add(
            DoctorModel(
                id = "doctorId",
                name = "TEST2",
                rating = 3.0
            )
        )
        list.add(
            DoctorModel(
                id = "doctorId",
                name = "TEST1",
                rating = 5.0
            )
        )
        list.add(
            DoctorModel(
                id = "doctorId",
                name = "TEST3",
                rating = 4.0
            )
        )
        return list
    }
}
