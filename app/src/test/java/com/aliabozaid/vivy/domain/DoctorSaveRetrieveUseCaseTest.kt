package com.aliabozaid.vivy.domain

import com.aliabozaid.vivy.base.UnitTest
import com.aliabozaid.vivy.data.model.DoctorModel
import com.aliabozaid.vivy.data.repository.DoctorSaveRetrieveRepository
import com.aliabozaid.vivy.domain.usecase.DoctorSaveRetrieveUseCase
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class DoctorSaveRetrieveUseCaseTest : UnitTest() {
    private lateinit var doctorSaveRetrieveUseCase: DoctorSaveRetrieveUseCase
    @Mock
    private lateinit var doctorSaveRetrieveRepository: DoctorSaveRetrieveRepository

    @Mock
    private lateinit var doctorsModel: List<DoctorModel>

    @Before
    fun setUp() {
        doctorSaveRetrieveUseCase = DoctorSaveRetrieveUseCase(doctorSaveRetrieveRepository)
    }

    @Test
    fun `running loadRecentSelectedDoctors should use loadRecentSelectedDoctors in doctorSaveRetrieveRepository`() {
        val myScope = GlobalScope
        runBlocking {
            myScope.launch(Dispatchers.IO) {
                doctorSaveRetrieveUseCase.getRecentDoctorModel()
                verify(doctorSaveRetrieveRepository).loadRecentSelectedDoctors()
            }
        }
    }

    @Test
    fun `running saveRecentDoctorModel should use saveRecentSelectedDoctors in doctorSaveRetrieveRepository`() {
        val myScope = GlobalScope
        runBlocking {
            myScope.launch(Dispatchers.IO) {
                doctorSaveRetrieveUseCase.saveRecentDoctorModel(
                    DoctorSaveRetrieveUseCase.Params(
                        doctorsModel
                    )
                )
                verify(doctorSaveRetrieveRepository).saveRecentSelectedDoctors(doctorsModel)
            }
        }
    }
}
