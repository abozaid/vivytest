package com.aliabozaid.vivy.domain

import com.aliabozaid.vivy.base.UnitTest
import com.aliabozaid.vivy.data.repository.DoctorsRepository
import com.aliabozaid.vivy.domain.usecase.DoctorsUseCase
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class DoctorsUseCaseTest : UnitTest() {
    private lateinit var doctorsUseCase: DoctorsUseCase
    @Mock
    private lateinit var doctorsRepository: DoctorsRepository

    @Before
    fun setUp() {
        doctorsUseCase = DoctorsUseCase(doctorsRepository)
    }

    @Test
    fun `running getDoctors should use DoctorsRepository and get data from repository`() {
        val myScope = GlobalScope
        runBlocking {
            myScope.launch(Dispatchers.IO) {
                doctorsUseCase.getDoctors(DoctorsUseCase.Params(API_KEY))
                verify(doctorsRepository).getDoctors(API_KEY)
                verifyNoMoreInteractions(doctorsRepository)
            }
        }
    }

    companion object {
        private const val API_KEY: String = "API_KEY"
    }
}
