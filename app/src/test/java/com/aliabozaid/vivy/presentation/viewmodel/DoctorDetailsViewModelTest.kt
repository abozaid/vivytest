package com.aliabozaid.vivy.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.aliabozaid.vivy.base.UnitTest
import com.aliabozaid.vivy.data.model.DoctorModel
import com.aliabozaid.vivy.domain.DoctorsGeneratorUtils
import com.aliabozaid.vivy.domain.usecase.DoctorSaveRetrieveUseCase
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock

class DoctorDetailsViewModelTest : UnitTest() {

    private val doctorModel = DoctorModel(
        id = "doctorId",
        name = "TEST2",
        rating = 3.0
    )

    @Mock
    private lateinit var doctorDetailsUseCase: DoctorSaveRetrieveUseCase

    private lateinit var doctorsViewModel: DoctorDetailsViewModel

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        doctorsViewModel = DoctorDetailsViewModel(
            doctorModel,
            doctorDetailsUseCase
        )
        doctorsViewModel.recentSelectedDoctors = DoctorsGeneratorUtils.getDoctorsModel()
    }

    @Test
    fun `create the object should use DoctorDetailsUseCase and get data from UseCase`() {
        val myScope = GlobalScope
        runBlocking {
            myScope.launch(Dispatchers.IO) {
                verify(doctorDetailsUseCase).getRecentDoctorModel()
                verify(doctorDetailsUseCase).saveRecentDoctorModel(
                    DoctorSaveRetrieveUseCase.Params(
                        DoctorsGeneratorUtils.getDoctorsModel()
                    )
                )
            }
        }
    }
}
