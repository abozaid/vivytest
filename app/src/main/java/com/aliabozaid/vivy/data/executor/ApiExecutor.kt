package com.aliabozaid.vivy.data.executor

import com.aliabozaid.vivy.data.exception.Failure
import com.aliabozaid.vivy.data.functional.Either
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ApiExecutor<Type> {
    suspend operator fun invoke(
        apiCallBlock: suspend () -> Type
    ): Either<Failure, Type> =
        withContext(Dispatchers.IO) {
            try {
                val result = apiCallBlock()
                Either.Right(result)
            } catch (e: Exception) {
                Either.Left(Failure.NetworkFailures())
            }
        }
}
