package com.aliabozaid.vivy.data.service

import com.aliabozaid.vivy.data.entity.toModel

class DoctorsService constructor(private val doctorsApi: DoctorsApi) {
    suspend fun getDoctors(apiKey: String) =
        doctorsApi.getDoctors(apiKey).toModel()
}
