package com.aliabozaid.vivy.data.repository

import com.aliabozaid.vivy.data.exception.Failure
import com.aliabozaid.vivy.data.executor.ApiExecutor
import com.aliabozaid.vivy.data.functional.Either
import com.aliabozaid.vivy.data.model.DoctorListModel
import com.aliabozaid.vivy.data.network.NetworkHandler
import com.aliabozaid.vivy.data.network.withNetwork
import com.aliabozaid.vivy.data.service.DoctorsService

class DoctorsRepository(
    private val apiExecutor: ApiExecutor<DoctorListModel>,
    private val networkHandler: NetworkHandler,
    private val doctorsService: DoctorsService
) {

    suspend fun getDoctors(apiKey: String): Either<Failure, DoctorListModel> =
        networkHandler.withNetwork {
            apiExecutor.invoke {
                doctorsService.getDoctors(apiKey)
            }
        }
}
