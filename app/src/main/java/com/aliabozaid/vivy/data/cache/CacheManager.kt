package com.aliabozaid.vivy.data.cache

import android.content.Context
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import java.io.File
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CacheManager(
    private val context: Context,
    val gson: Gson
) {

    fun getCachedFile(key: String) = File(context.cacheDir, key)

    suspend fun saveDataAsync(
        key: String,
        objectToSave: Any
    ) =
        withContext(Dispatchers.IO) {
            val data = gson.toJson(objectToSave)
            getCachedFile(key)
                .writeText(data)
        }

    suspend inline fun <reified T : Any?> loadData(
        key: String
    ): T? =
        withContext(Dispatchers.IO) {
            getCachedFile(key)
                .takeIf { it.exists() }
                ?.let {
                    try {
                        gson.fromJson(
                            it.readText(),
                            object : TypeToken<T>() {}.type
                        ) as T?
                    } catch (t: JsonSyntaxException) {
                        null
                    }
                }
        }
}
