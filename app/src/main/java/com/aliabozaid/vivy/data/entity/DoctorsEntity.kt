package com.aliabozaid.vivy.data.entity

import com.aliabozaid.vivy.data.model.DoctorListModel
import com.aliabozaid.vivy.data.model.DoctorModel

data class DoctorEntity(
    val id: String?,
    val name: String?,
    val photoId: String?,
    val rating: Double?,
    val address: String?,
    val lat: Double?,
    val lng: Double?,
    val highlighted: Boolean?,
    val reviewCount: Int?,
    val specialityIds: List<Int>?,
    val source: String?,
    val phoneNumber: String?,
    val email: String?,
    val website: String?,
    val openingHours: List<String>?,
    val integration: String?,
    val translation: String?
)

data class DoctorsListEntity(
    val doctors: List<DoctorEntity>,
    val lastKey: String?
)

fun DoctorsListEntity.toModel(): DoctorListModel =
    DoctorListModel(
        doctors = doctors.map { doctor ->
            DoctorModel(
                id = doctor.id,
                name = doctor.name,
                photoId = doctor.photoId,
                rating = doctor.rating,
                address = doctor.address,
                lat = doctor.lat,
                lng = doctor.lng,
                highlighted = doctor.highlighted,
                reviewCount = doctor.reviewCount,
                specialityIds = doctor.specialityIds,
                source = doctor.source,
                phoneNumber = doctor.phoneNumber,
                email = doctor.email,
                website = doctor.website,
                openingHours = doctor.openingHours,
                integration = doctor.integration,
                translation = doctor.translation
            )
        }.toMutableList(),
        lastKey = lastKey
    )
