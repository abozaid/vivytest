package com.aliabozaid.vivy.data.service

import com.aliabozaid.vivy.data.cache.CacheManager
import com.aliabozaid.vivy.data.entity.DoctorCacheEntity
import com.aliabozaid.vivy.data.entity.toCacheEntity
import com.aliabozaid.vivy.data.entity.toDoctorModel
import com.aliabozaid.vivy.data.model.DoctorModel

class DoctorDetailsService(
    private val cacheManager: CacheManager
) {
    suspend fun loadRecentSelectedDoctors(): List<DoctorModel>? =
        cacheManager.loadData<List<DoctorCacheEntity>>(DOCTOR_ENTITY_CACHE_KEY)?.toDoctorModel()

    suspend fun saveRecentSelectedDoctors(doctorEntities: List<DoctorModel>) {
        cacheManager.saveDataAsync(DOCTOR_ENTITY_CACHE_KEY, doctorEntities.toCacheEntity())
    }

    companion object {
        private const val DOCTOR_ENTITY_CACHE_KEY = "DOCTOR_ENTITY_CACHE_KEY"
    }
}
