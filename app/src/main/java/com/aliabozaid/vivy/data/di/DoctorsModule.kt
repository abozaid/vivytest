package com.aliabozaid.vivy.data.di

import com.aliabozaid.vivy.data.cache.CacheManager
import com.aliabozaid.vivy.data.executor.ApiExecutor
import com.aliabozaid.vivy.data.network.NetworkHandler
import com.aliabozaid.vivy.data.network.NetworkHandlerImpl
import com.aliabozaid.vivy.data.repository.DoctorSaveRetrieveRepository
import com.aliabozaid.vivy.data.repository.DoctorsRepository
import com.aliabozaid.vivy.data.service.DoctorDetailsService
import com.aliabozaid.vivy.data.service.DoctorsApi
import com.aliabozaid.vivy.data.service.DoctorsService
import com.aliabozaid.vivy.domain.usecase.DoctorSaveRetrieveUseCase
import com.aliabozaid.vivy.domain.usecase.DoctorsUseCase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

val domainModule = module {
    single { DoctorsUseCase(get()) }
    single { DoctorSaveRetrieveUseCase(get()) }
}

val dataModule = module {
    single { NetworkHandlerImpl(androidContext()) } bind NetworkHandler::class
    single { ApiExecutor<Any>() }
    single { get<Retrofit>().create(DoctorsApi::class.java) }
    single { DoctorsService(get()) }
    single { DoctorsRepository(get(), get(), get()) }
    single { CacheManager(androidContext(), get()) }
    single { DoctorDetailsService(get()) }
    single { DoctorSaveRetrieveRepository(get()) }
}
