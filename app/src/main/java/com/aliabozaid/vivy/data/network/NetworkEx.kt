package com.aliabozaid.vivy.data.network

import com.aliabozaid.vivy.data.exception.Failure
import com.aliabozaid.vivy.data.functional.Either

suspend fun <T> NetworkHandler.withNetwork(callback: suspend () -> Either<Failure, T>): Either<Failure, T> {
    return if (isConnected()) {
        callback()
    } else {
        Either.Left(Failure.NetworkConnection)
    }
}
