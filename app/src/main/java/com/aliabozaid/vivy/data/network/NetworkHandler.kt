package com.aliabozaid.vivy.data.network

interface NetworkHandler {
    fun isConnected(): Boolean
}
