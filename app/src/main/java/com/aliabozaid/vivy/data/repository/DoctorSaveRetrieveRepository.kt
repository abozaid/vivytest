package com.aliabozaid.vivy.data.repository

import com.aliabozaid.vivy.data.model.DoctorModel
import com.aliabozaid.vivy.data.service.DoctorDetailsService

class DoctorSaveRetrieveRepository(
    private val doctorDetailsService: DoctorDetailsService
) {
    suspend fun loadRecentSelectedDoctors(): List<DoctorModel> =
        doctorDetailsService.loadRecentSelectedDoctors() ?: emptyList()

    suspend fun saveRecentSelectedDoctors(doctorsModel: List<DoctorModel>) {
        doctorDetailsService.saveRecentSelectedDoctors(doctorsModel)
    }
}
