package com.aliabozaid.vivy.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DoctorModel(
    val id: String? = null,
    val name: String? = null,
    val photoId: String? = null,
    val rating: Double? = null,
    val address: String? = null,
    val lat: Double? = null,
    val lng: Double? = null,
    val highlighted: Boolean? = null,
    val reviewCount: Int? = null,
    val specialityIds: List<Int>? = null,
    val source: String? = null,
    val phoneNumber: String? = null,
    val email: String? = null,
    val website: String? = null,
    val openingHours: List<String>? = null,
    val integration: String? = null,
    val translation: String? = null
) : Parcelable {

    fun isSame(other: DoctorModel): Boolean = this.id == other.id

    fun isContentSame(other: DoctorModel) = this == other
}

data class DoctorListModel(
    val doctors: MutableList<DoctorModel>,
    val lastKey: String?
)
