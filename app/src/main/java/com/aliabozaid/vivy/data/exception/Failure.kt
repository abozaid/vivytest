package com.aliabozaid.vivy.data.exception

/**
 * Base Class for handling errors/failures/exceptions.
 * Every feature specific failure should extend [FeatureFailure] class.
 */
sealed class Failure : Exception() {
    object NetworkConnection : Failure()

    /** * Extend this class for network specific failures.*/
    abstract class ServerError : Failure()

    class NetworkFailures : Failure.ServerError()

    /** * Extend this class for feature specific failures.*/
    abstract class FeatureFailure : Failure()
}
