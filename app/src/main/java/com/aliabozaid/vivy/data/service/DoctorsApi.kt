package com.aliabozaid.vivy.data.service

import com.aliabozaid.vivy.data.entity.DoctorsListEntity
import retrofit2.http.GET
import retrofit2.http.Path

interface DoctorsApi {
    companion object {
        private const val DOCTORS_PATH = "android/{apiKey}"
    }

    @GET(DOCTORS_PATH)
    suspend fun getDoctors(
        @Path("apiKey") path: String
    ): DoctorsListEntity
}
