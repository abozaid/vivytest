package com.aliabozaid.vivy.data.entity

import com.aliabozaid.vivy.data.model.DoctorModel

data class DoctorCacheEntity(
    val id: String?,
    val name: String?,
    val photoId: String?,
    val rating: Double?,
    val address: String?,
    val lat: Double?,
    val lng: Double?,
    val highlighted: Boolean?,
    val reviewCount: Int?,
    val specialityIds: List<Int>?,
    val source: String?,
    val phoneNumber: String?,
    val email: String?,
    val website: String?,
    val openingHours: List<String>?,
    val integration: String?,
    val translation: String?
)

fun List<DoctorModel>.toCacheEntity(): List<DoctorCacheEntity> =
    map { doctorModel ->
        DoctorCacheEntity(
            doctorModel.id,
            doctorModel.name,
            doctorModel.photoId,
            doctorModel.rating,
            doctorModel.address,
            doctorModel.lat,
            doctorModel.lng,
            doctorModel.highlighted,
            doctorModel.reviewCount,
            doctorModel.specialityIds,
            doctorModel.source,
            doctorModel.phoneNumber,
            doctorModel.email,
            doctorModel.website,
            doctorModel.openingHours,
            doctorModel.integration,
            doctorModel.translation
        )
    }

fun List<DoctorCacheEntity>.toDoctorModel(): List<DoctorModel> =
    map { doctorCacheEntity ->
        DoctorModel(
            doctorCacheEntity.id,
            doctorCacheEntity.name,
            doctorCacheEntity.photoId,
            doctorCacheEntity.rating,
            doctorCacheEntity.address,
            doctorCacheEntity.lat,
            doctorCacheEntity.lng,
            doctorCacheEntity.highlighted,
            doctorCacheEntity.reviewCount,
            doctorCacheEntity.specialityIds,
            doctorCacheEntity.source,
            doctorCacheEntity.phoneNumber,
            doctorCacheEntity.email,
            doctorCacheEntity.website,
            doctorCacheEntity.openingHours,
            doctorCacheEntity.integration,
            doctorCacheEntity.translation
        )
    }
