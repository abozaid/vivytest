package com.aliabozaid.vivy.data.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.annotation.IntRange

@Suppress("DEPRECATION")
class NetworkHandlerImpl constructor(private val context: Context) : NetworkHandler {

    override fun isConnected(): Boolean = when (getConnectionType()) {
        1, 2 -> true
        else -> false
    }

    @IntRange(from = 0, to = 2)
    private fun getConnectionType(): Int {
        var result = 0 // Returns connection type. 0: none; 1: mobile data; 2: wifi
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            cm?.run {
                cm.getNetworkCapabilities(cm.activeNetwork)?.run {
                    if (hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        result = 2 // wifi
                    } else if (hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        result = 1 // mobile
                    }
                }
            }
        } else {
            cm?.run {
                cm.activeNetworkInfo?.run {
                    if (type == ConnectivityManager.TYPE_WIFI) {
                        result = 2 // wifi
                    } else if (type == ConnectivityManager.TYPE_MOBILE) {
                        result = 1 // mobile
                    }
                }
            }
        }
        return result
    }
}
