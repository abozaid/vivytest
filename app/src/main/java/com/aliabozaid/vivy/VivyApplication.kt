package com.aliabozaid.vivy

import android.app.Application
import com.aliabozaid.vivy.data.di.dataModule
import com.aliabozaid.vivy.data.di.domainModule
import com.aliabozaid.vivy.data.di.networkModule
import com.aliabozaid.vivy.di.viewModelsModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class VivyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            // use AndroidLogger as Koin Logger - default Level.INFO
            androidLogger()

            // use the Android context given there
            androidContext(this@VivyApplication)

            // modules
            modules(listOf(networkModule,
                dataModule,
                domainModule, viewModelsModule))
        }
    }
}
