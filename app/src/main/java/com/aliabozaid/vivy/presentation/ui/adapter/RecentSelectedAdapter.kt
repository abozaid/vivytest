package com.aliabozaid.vivy.presentation.ui.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aliabozaid.vivy.R
import com.aliabozaid.vivy.data.model.DoctorModel
import com.aliabozaid.vivy.utils.inflate
import com.aliabozaid.vivy.utils.load
import kotlinx.android.synthetic.main.view_doctor_row.view.*
import kotlinx.android.synthetic.main.view_header_row.view.*

class RecentSelectedAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var itemSelectedListener: ((itemView: View, DoctorModel?) -> Unit)? = null
    private var items: List<DoctorModel>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        if (viewType == TYPE_HEADER) {
            HeaderViewHolder(
                parent.inflate(R.layout.view_header_row, false)
            )
        } else {
            RecentSelectedViewHolder(
                parent.inflate(R.layout.view_doctor_row, false)
            )
        }

    override fun getItemCount(): Int = items?.size?.plus(1) ?: 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is RecentSelectedViewHolder -> {
                holder.bind(items?.get(position.minus(1)))
            }
        }
    }

    override fun getItemViewType(position: Int): Int =
        if (position == 0) {
            TYPE_HEADER
        } else {
            TYPE_ITEM
        }

    fun setRecentSelectedDoctors(items: List<DoctorModel>) {
        this.items = items
    }

    inner class RecentSelectedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(
            doctorModel: DoctorModel?
        ) = with(itemView) {
            doctorName.text = doctorModel?.name
            doctorAddress.text = doctorModel?.address
            doctorIcon.load(doctorModel?.photoId)
            itemView.setOnClickListener {
                itemSelectedListener?.invoke(itemView, doctorModel)
            }
        }
    }

    private class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            with(itemView) {
                header.setText(R.string.recent_doctors)
            }
        }
    }

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
    }
}
