package com.aliabozaid.vivy.presentation.datasource.factory

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.aliabozaid.vivy.data.model.DoctorModel
import com.aliabozaid.vivy.domain.usecase.DoctorsUseCase
import com.aliabozaid.vivy.presentation.datasource.DoctorsDataSource
import kotlinx.coroutines.CoroutineScope

class DoctorsDataFactory(
    private val doctorsUseCase: DoctorsUseCase,
    private val scope: CoroutineScope
) : DataSource.Factory<String, DoctorModel>() {
    private val mutableLiveData: MutableLiveData<DoctorsDataSource> = MutableLiveData()
    private lateinit var dataSourceClass: DoctorsDataSource

    override fun create(): DataSource<String, DoctorModel> {
        dataSourceClass = DoctorsDataSource(doctorsUseCase, scope)
        mutableLiveData.postValue(dataSourceClass)
        return dataSourceClass
    }

    fun getMutableLiveData(): MutableLiveData<DoctorsDataSource> {
        return mutableLiveData
    }
}
