package com.aliabozaid.vivy.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aliabozaid.vivy.data.model.DoctorModel
import com.aliabozaid.vivy.domain.usecase.DoctorSaveRetrieveUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DoctorDetailsViewModel(
    private val doctorModel: DoctorModel,
    private val doctorDetailsUseCase: DoctorSaveRetrieveUseCase
) : ViewModel() {
    lateinit var recentSelectedDoctors: MutableList<DoctorModel>

    private val _doctorModelLiveData: MutableLiveData<DoctorModel> = MutableLiveData()
    val doctorModelLiveData: LiveData<DoctorModel> = _doctorModelLiveData

    init {
        getRecentlySelectedDoctors()
        _doctorModelLiveData.postValue(doctorModel)
    }

    private fun getRecentlySelectedDoctors() {
        viewModelScope.launch(Dispatchers.IO) {
            recentSelectedDoctors = doctorDetailsUseCase.getRecentDoctorModel().toMutableList()
            checkCountToSave()
        }
    }

    private fun checkCountToSave() {
        recentSelectedDoctors.remove(recentSelectedDoctors.firstOrNull { it.id.equals(doctorModel.id) })
        if (recentSelectedDoctors.size >= RECENTLY_VIEWED_DOCTORS_LIMIT) {
            recentSelectedDoctors.removeAt(recentSelectedDoctors.lastIndex)
        }
        recentSelectedDoctors.add(0, doctorModel)
        saveRecentlySelectedDoctors()
    }

    private fun saveRecentlySelectedDoctors() {
        viewModelScope.launch {
            doctorDetailsUseCase.saveRecentDoctorModel(
                DoctorSaveRetrieveUseCase.Params(
                    recentSelectedDoctors
                )
            )
        }
    }

    companion object {
        const val RECENTLY_VIEWED_DOCTORS_LIMIT = 3
    }
}
