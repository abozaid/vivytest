package com.aliabozaid.vivy.presentation.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.aliabozaid.vivy.data.model.DoctorModel
import com.aliabozaid.vivy.domain.usecase.DoctorsUseCase
import com.aliabozaid.vivy.presentation.state.LiveDataState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DoctorsDataSource(
    private val doctorsUseCase: DoctorsUseCase,
    private val scope: CoroutineScope
) : PageKeyedDataSource<String, DoctorModel>() {
    val liveDataState: MutableLiveData<LiveDataState> = MutableLiveData()

    override fun loadInitial(
        params: LoadInitialParams<String>,
        callback: LoadInitialCallback<String, DoctorModel>
    ) {
        scope.launch(Dispatchers.IO) {
            liveDataState.postValue(LiveDataState.loading())
            doctorsUseCase.getDoctors(
                DoctorsUseCase.Params(null)
            )
                .either({
                    liveDataState.postValue(LiveDataState.error(it))
                }, {
                    callback.onResult(it.doctors, null, it.lastKey)
                    liveDataState.postValue(LiveDataState.success())
                })
        }
    }

    override fun loadAfter(
        params: LoadParams<String>,
        callback: LoadCallback<String, DoctorModel>
    ) {

        scope.launch(Dispatchers.IO) {
            liveDataState.postValue(LiveDataState.loading())
            doctorsUseCase.getDoctors(
                DoctorsUseCase.Params(
                    params.key
                )
            ).either({
                liveDataState.postValue(LiveDataState.error(it))
            }, {
                callback.onResult(it.doctors, it.lastKey)
                liveDataState.postValue(LiveDataState.success())
            })
        }
    }

    override fun loadBefore(
        params: LoadParams<String>,
        callback: LoadCallback<String, DoctorModel>
    ) {
    }
}
