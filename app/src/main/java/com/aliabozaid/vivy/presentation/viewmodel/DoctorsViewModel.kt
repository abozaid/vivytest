package com.aliabozaid.vivy.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.aliabozaid.vivy.data.model.DoctorModel
import com.aliabozaid.vivy.domain.usecase.DoctorSaveRetrieveUseCase
import com.aliabozaid.vivy.domain.usecase.DoctorsUseCase
import com.aliabozaid.vivy.presentation.datasource.DoctorsDataSource
import com.aliabozaid.vivy.presentation.datasource.factory.DoctorsDataFactory
import com.aliabozaid.vivy.presentation.state.LiveDataState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DoctorsViewModel(
    private val doctorsUseCase: DoctorsUseCase,
    private val doctorSaveRetrieveUseCase: DoctorSaveRetrieveUseCase
) : ViewModel() {

    private val _doctorsModelLiveData: MutableLiveData<List<DoctorModel>> = MutableLiveData()
    val doctorsModelLiveData: LiveData<List<DoctorModel>> = _doctorsModelLiveData

    lateinit var doctorsLiveData: LiveData<PagedList<DoctorModel>>
    private val doctorsDataFactory: DoctorsDataFactory by lazy {
        DoctorsDataFactory(doctorsUseCase, viewModelScope)
    }

    init {
        buildTrendingPageBuilder()
    }

    fun getTrendingLiveDataState(): LiveData<LiveDataState> {
        return Transformations.switchMap(
            doctorsDataFactory.getMutableLiveData(),
            DoctorsDataSource::liveDataState
        )
    }

    private fun buildTrendingPageBuilder() {

        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(PAGES_SIZE)
            .build()

        doctorsLiveData = LivePagedListBuilder(
            doctorsDataFactory,
            pagedListConfig
        ).build()
    }

    fun loadRecentDoctors() {
        viewModelScope.launch(Dispatchers.IO) {
            val doctors = doctorSaveRetrieveUseCase.getRecentDoctorModel()
            if (doctors.isNotEmpty()) {
                _doctorsModelLiveData.postValue(doctors)
            }
        }
    }

    companion object {
        const val PAGES_SIZE = 10
    }
}
