package com.aliabozaid.vivy.presentation.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.aliabozaid.vivy.R
import com.aliabozaid.vivy.data.model.DoctorModel
import com.aliabozaid.vivy.presentation.viewmodel.DoctorDetailsViewModel
import com.aliabozaid.vivy.utils.load
import kotlinx.android.synthetic.main.activity_details.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class DoctorDetailsActivity : AppCompatActivity() {

    private val viewModel: DoctorDetailsViewModel by viewModel {
        parametersOf(doctorModel)
    }

    private val doctorModel: DoctorModel? by lazy {
        intent.extras?.getParcelable(DOCTOR_MODEL) as? DoctorModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.doctorModelLiveData.observe(this, Observer { doctorModel ->
            doctorIcon.load(doctorModel?.photoId)
            doctorName.text = doctorModel?.name
            doctorAddress.text = doctorModel?.address
        })
    }

    companion object {
        private const val DOCTOR_MODEL = "doctorModel"
        fun intent(context: Context, doctorModel: DoctorModel?): Intent {
            return Intent(context, DoctorDetailsActivity::class.java).apply {
                putExtra(DOCTOR_MODEL, doctorModel)
            }
        }
    }
}
