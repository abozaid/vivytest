package com.aliabozaid.vivy.presentation.state

class LiveDataState private constructor(
    val status: ResultState,
    val throwable: Throwable? = null
) {

    companion object {

        fun error(error: Throwable?): LiveDataState = LiveDataState(ResultState.ERROR, error)

        fun loading(): LiveDataState = LiveDataState(ResultState.LOADING)

        fun success(): LiveDataState = LiveDataState(ResultState.SUCCESS)
    }
}

enum class ResultState {
    LOADING,
    SUCCESS,
    ERROR
}
