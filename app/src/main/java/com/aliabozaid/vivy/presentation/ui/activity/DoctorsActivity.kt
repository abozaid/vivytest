package com.aliabozaid.vivy.presentation.ui.activity

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.aliabozaid.vivy.R
import com.aliabozaid.vivy.presentation.state.ResultState
import com.aliabozaid.vivy.presentation.ui.adapter.DoctorsAdapter
import com.aliabozaid.vivy.presentation.ui.adapter.RecentSelectedAdapter
import com.aliabozaid.vivy.presentation.viewmodel.DoctorsViewModel
import com.aliabozaid.vivy.utils.showHide
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DoctorsActivity : AppCompatActivity() {

    private val viewModel: DoctorsViewModel by viewModel()
    private lateinit var adapter: DoctorsAdapter
    private lateinit var recentAdapter: RecentSelectedAdapter

    override fun onStart() {
        super.onStart()
        viewModel.loadRecentDoctors()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = DoctorsAdapter()
        recentAdapter =
            RecentSelectedAdapter()

        doctorsRecyclerView.apply {
            adapter = this@DoctorsActivity.adapter
            layoutManager = LinearLayoutManager(this@DoctorsActivity)

            isNestedScrollingEnabled = true
            setHasFixedSize(true)
        }

        recentVisitedDoctorsRecyclerView.apply {
            adapter = recentAdapter
            layoutManager = LinearLayoutManager(context)
            isNestedScrollingEnabled = false
            setHasFixedSize(true)
        }

        observeViewModel()
        initListeners()
    }

    private fun initListeners() {
        adapter.itemSelectedListener = { _, doctorModel ->
            startActivity(DoctorDetailsActivity.intent(this, doctorModel))
            // viewModel.invalidateData()
        }
        recentAdapter.itemSelectedListener = { _, doctorModel ->
            startActivity(DoctorDetailsActivity.intent(this, doctorModel))
        }
    }

    private fun observeViewModel() {
        initCreateCaseLiveData()

        viewModel.doctorsLiveData.observe(this, Observer {
            adapter.submitList(it)
        })
        viewModel.doctorsModelLiveData.observe(this, Observer {
            appBarLayout.showHide(true)
            recentAdapter.setRecentSelectedDoctors(it)
            recentAdapter.notifyDataSetChanged()
        })
    }

    private fun initCreateCaseLiveData() {
        viewModel.getTrendingLiveDataState().observe(this, Observer {
            when (it?.status) {
                ResultState.LOADING -> {
                    showProgress(true)
                }
                ResultState.ERROR -> {
                    showProgress(false)
                    Toast.makeText(this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show()
                }
                ResultState.SUCCESS -> {
                    doctorsRecyclerView.showHide(true)
                    showProgress(false)
                }
            }
        })
    }

    private fun showProgress(show: Boolean) {
        progressBar.showHide(show)
    }
}
