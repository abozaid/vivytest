package com.aliabozaid.vivy.domain.usecase

import com.aliabozaid.vivy.data.model.DoctorModel
import com.aliabozaid.vivy.data.repository.DoctorSaveRetrieveRepository

class DoctorSaveRetrieveUseCase(
    private val doctorSaveRetrieveRepository: DoctorSaveRetrieveRepository
) {
    suspend fun getRecentDoctorModel(): List<DoctorModel> =
        doctorSaveRetrieveRepository.loadRecentSelectedDoctors()

    suspend fun saveRecentDoctorModel(params: Params) {
        doctorSaveRetrieveRepository.saveRecentSelectedDoctors(params.doctorsModel)
    }

    data class Params(
        val doctorsModel: List<DoctorModel>
    )
}
