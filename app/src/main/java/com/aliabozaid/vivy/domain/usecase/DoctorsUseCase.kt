package com.aliabozaid.vivy.domain.usecase

import com.aliabozaid.vivy.data.exception.Failure
import com.aliabozaid.vivy.data.functional.Either
import com.aliabozaid.vivy.data.model.DoctorListModel
import com.aliabozaid.vivy.data.repository.DoctorsRepository

class DoctorsUseCase(
    private val doctorsRepository: DoctorsRepository
) {

    suspend fun getDoctors(params: Params): Either<Failure, DoctorListModel> {
        val key = if (params.apiKey == null) {
            "$DOCTORS_PREFIX$DOCTORS_SUFFIX"
        } else {
            "$DOCTORS_PREFIX-${params.apiKey}$DOCTORS_SUFFIX"
        }
        val data = doctorsRepository.getDoctors(key)
        data.either({}, {
            it.doctors.sortByDescending { doctorModel -> doctorModel.rating ?: 0.0 }
        })
        return data
    }

    data class Params(
        val apiKey: String?
    )

    companion object {
        const val DOCTORS_PREFIX = "doctors"
        const val DOCTORS_SUFFIX = ".json"
    }
}
