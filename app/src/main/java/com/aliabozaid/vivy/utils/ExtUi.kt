package com.aliabozaid.vivy.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.LayoutRes
import com.aliabozaid.vivy.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun View.showHide(show: Boolean) {
    visibility = if (show) View.VISIBLE else View.GONE
}

fun ImageView.load(url: String?) {

    Glide.with(context)
        .load(url)
        .fitCenter()
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .placeholder(R.drawable.place_holder)
        .error(R.drawable.place_holder)
        .into(this)
}
