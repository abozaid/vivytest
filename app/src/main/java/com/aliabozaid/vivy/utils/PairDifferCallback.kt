package com.aliabozaid.vivy.utils

import androidx.recyclerview.widget.DiffUtil
import com.aliabozaid.vivy.data.model.DoctorModel

class PairDifferCallback : DiffUtil.ItemCallback<DoctorModel>() {
    override fun areContentsTheSame(oldItem: DoctorModel, newItem: DoctorModel): Boolean =
        oldItem.isContentSame(newItem)

    override fun areItemsTheSame(oldItem: DoctorModel, newItem: DoctorModel): Boolean =
        oldItem.isSame(newItem)
}
