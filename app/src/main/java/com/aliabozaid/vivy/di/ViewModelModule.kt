package com.aliabozaid.vivy.di

import com.aliabozaid.vivy.data.model.DoctorModel
import com.aliabozaid.vivy.presentation.viewmodel.DoctorDetailsViewModel
import com.aliabozaid.vivy.presentation.viewmodel.DoctorsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val viewModelsModule: Module = module {

    viewModel { DoctorsViewModel(get(), get()) }
    viewModel { (doctorModel: DoctorModel) ->
        DoctorDetailsViewModel(
            doctorModel,
            get()
        )
    }
}
